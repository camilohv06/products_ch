# Productos CH Con Jetpack Compose

Aplicación la cual consulta productos de la Api de mercadolibre (https://api.mercadolibre.com)

# Requerimientos Mínimos
- minSdk 26
- targetSdk 30

# Librerias utilizadas
- Coil: Cargar Imagenes
- Retrofit: Peticiones al servidor
- Dagger: Inyeccion de dependencias
- Accompanist: Manejo de permisos
- JUnit: Pruebas Unitarias

# Funcionalidades
- Consulta de productos por defecto
- Búsqueda de productos a partir de un criterio de búsqueda ingresado por el usuario
- Visualización en detalle de un producto seleccionado


# Architecture
![Screenshot1](https://bitbucket.org/camilohv06/products_ch/raw/9ff6c7215e8646796226516177d2c5a5fbbf8d08/screenshots/diagram.png?raw=true)