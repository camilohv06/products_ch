package com.camilo.products.utils

import com.camilo.products.di.RepositoryModule
import com.camilo.products.model.Product
import com.camilo.products.repository.ProductsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import javax.inject.Singleton

@Module
@TestInstallIn(
    components = [SingletonComponent::class],
    replaces = [RepositoryModule::class]
)
class FakeRepositoryModule {
    @Provides
    @Singleton
    fun providerProductsRepository(): ProductsRepository =
        object : ProductsRepository {
            val products = Resource.Success(
                listOf(
                    Product(
                        "MCO540523749", "Whisky", 12900,
                        "http://http2.mlstatic.com/D_751658-MCO46597556582_072021-I.jpg",
                        "https://articulo.mercadolibre.com.co/MCO-540523749-whisky-buchanans-deluxe-750ml-_JM"
                    )
                )
            )

            override suspend fun getProductsByCategory(): Resource<List<Product>> =
                Resource.Success(products.data!!)

            override suspend fun getProductsByQuery(query: String): Resource<List<Product>> =
                Resource.Success(products.data!!)

            override fun getProduct(title: String): Product = products.data!!.get(0)


        }
}