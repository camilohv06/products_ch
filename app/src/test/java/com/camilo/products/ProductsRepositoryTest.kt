package com.camilo.products

import com.camilo.products.provider.ProductsProvider
import com.camilo.products.repository.ProductsRepositoryImpl
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThrows
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.nio.charset.StandardCharsets

class ProductsRepositoryTest {
    private val mockWebServer = MockWebServer()

    private val productsProvider = Retrofit.Builder()
        .baseUrl(mockWebServer.url("/"))
        .client(OkHttpClient.Builder().build())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ProductsProvider::class.java)

    private val productsRepository = ProductsRepositoryImpl(productsProvider)

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `Response Api is Correct`() {
        mockWebServer.enqueueResponse("products_test.json")

        runBlocking {
            val products = productsRepository.getProductsByCategory()
            assertEquals(1, products.data!!.size)
        }
    }

    @Test
    fun `Response Api is empty`() {
        mockWebServer.enqueueResponse("products_empty.json")
        assertThrows(Exception::class.java) {
            runBlocking {
                productsRepository.getProductsByCategory()
            }
        }
    }
}

fun MockWebServer.enqueueResponse(filePath: String) {
    val inputStream = javaClass.classLoader?.getResourceAsStream(filePath)
    val source = inputStream?.source()?.buffer()
    source?.let {
        enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(it.readString(StandardCharsets.UTF_8))
        )
    }
}