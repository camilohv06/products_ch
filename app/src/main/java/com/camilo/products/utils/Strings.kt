package com.camilo.products.utils

import androidx.annotation.StringRes
import com.camilo.products.ProductApplication

object Strings {
    fun get(@StringRes stringRes: Int, vararg formatArgs: Any = emptyArray()): String {
        return ProductApplication.instance.getString(stringRes, *formatArgs)
    }
}