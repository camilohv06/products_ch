package com.camilo.products

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.camilo.products.ui.theme.Product_CHTheme
import com.camilo.products.view.DetailsScreen
import com.camilo.products.view.ListScreen
import com.camilo.products.view.SearchScreen
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import dagger.hilt.android.AndroidEntryPoint

object Destinations {
    const val LIST_SCREEN = "LIST_SCREEN"
    const val DETAILS_SCREEN = "DETAILS_SCREEN"
    const val SEARCH_SCREEN = "SEARCH_SCREEN"
}

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    @ExperimentalPermissionsApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Product_CHTheme {
                Surface(color = MaterialTheme.colors.background) {
                    val navController = rememberNavController()
                    NavHost(
                        navController = navController,
                        startDestination = Destinations.LIST_SCREEN
                    ) {
                        composable(Destinations.LIST_SCREEN) {
                            ListScreen(navController)
                        }
                        composable("${Destinations.DETAILS_SCREEN}/{title}") {
                            it.arguments?.getString("title")?.let { title ->
                                DetailsScreen(title, navController)
                            }
                        }
                        composable(Destinations.SEARCH_SCREEN) {
                            SearchScreen(navController)
                        }
                    }
                }
            }
        }
    }
}


