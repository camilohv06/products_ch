package com.camilo.products.view.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.camilo.products.model.Product
import java.text.NumberFormat

@Composable
fun ProductCard(
    product: Product,
    onCLick: () -> Unit
) {
    Card(
        shape = RoundedCornerShape(8.dp),
        modifier = Modifier
            .padding(10.dp)
            .fillMaxWidth()
            .clickable(onClick = onCLick),
        elevation = 8.dp
    ) {
        Row(
            modifier = Modifier.padding(2.dp),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            ImageProduct(product = product)
            Column(Modifier.padding(5.dp)) {
                Text(product.title, fontSize = 20.sp, fontWeight = FontWeight.Bold)
                Text(
                    text = product.currency_id + " " + NumberFormat.getInstance()
                        .format(product.price),
                    fontSize = 18.sp
                )
            }
        }
    }
}