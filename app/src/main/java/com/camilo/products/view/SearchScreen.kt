package com.camilo.products.view

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.camilo.products.Destinations
import com.camilo.products.R
import com.camilo.products.model.Product
import com.camilo.products.ui.theme.Product_CHTheme
import com.camilo.products.utils.Strings
import com.camilo.products.view.components.*
import com.camilo.products.viewmodel.SearchScreenViewModel
import com.google.accompanist.permissions.ExperimentalPermissionsApi

@ExperimentalPermissionsApi
@Composable
fun SearchScreen(
    navController: NavController,
    viewModel: SearchScreenViewModel = hiltViewModel()
) {
    val productsList by remember { viewModel.productsFiltered }
    val isLoading by remember { viewModel.isLoading }
    val error by remember { viewModel.error }
    SearchScreen(navController, productsList, isLoading, error, viewModel)
}

@ExperimentalPermissionsApi
@Composable
fun SearchScreen(
    navController: NavController,
    products: List<Product>,
    isLoading: Boolean,
    error: String,
    viewModel: SearchScreenViewModel = hiltViewModel()
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text("Buscar") },
                navigationIcon = {
                    IconButton(onClick = { navController.popBackStack() }) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = Strings.get(R.string.lbl_back),
                        )
                    }
                }
            )
        },
    ) {
        val textState = remember { mutableStateOf(TextFieldValue(viewModel.query.value)) }
        Column {
            SearchView(state = textState)
            CircularProgressBar(isDisplayed = isLoading)
            MessageError(errorMsg = error)
            ListProduct(
                navController = navController,
                products = products,
                state = textState,
                viewModel = viewModel
            )
        }
    }
}

@Composable
fun ListProduct(
    navController: NavController,
    products: List<Product>,
    state: MutableState<TextFieldValue>,
    viewModel: SearchScreenViewModel
) {
    viewModel.getProductsByQuery(state.value.text)
    LazyColumn(contentPadding = PaddingValues(4.dp)) {
        items(products) { product ->
            ProductCard(
                product = product,
                onCLick = {
                    navController.navigate("${Destinations.DETAILS_SCREEN}/${product.title}")
                }
            )
        }
    }
}


@ExperimentalPermissionsApi
@Preview(showBackground = true)
@Composable
fun SearchPreview() {
    Product_CHTheme {
        SearchScreen(
            navController = rememberNavController(),
            products = listOf(
                Product(
                    "MCO540523749", "Whisky", 12900,
                    "http://http2.mlstatic.com/D_751658-MCO46597556582_072021-I.jpg",
                    "https://articulo.mercadolibre.com.co/MCO-540523749-whisky-buchanans-deluxe-750ml-_JM"
                )
            ),
            false, ""
        )
    }
}
