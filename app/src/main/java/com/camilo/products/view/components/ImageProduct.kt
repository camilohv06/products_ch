package com.camilo.products.view.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.camilo.products.R
import com.camilo.products.model.Product

@Composable
fun ImageProduct(
    product: Product
) {
    Image(
        modifier = Modifier
            .padding(5.dp)
            .size(70.dp)
            .clip(CircleShape)
            .border(2.dp, MaterialTheme.colors.primary, CircleShape),
        painter = rememberImagePainter(
            data = product.thumbnail,
            builder = {
                placeholder(R.drawable.placeholder)
                error(R.drawable.placeholder)
            }
        ),
        contentDescription = null,
        contentScale = ContentScale.Crop
    )
}