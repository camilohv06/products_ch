package com.camilo.products.view

import android.Manifest
import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.camilo.products.Destinations
import com.camilo.products.model.Product
import com.camilo.products.ui.theme.Product_CHTheme
import com.camilo.products.view.components.*
import com.camilo.products.viewmodel.ListScreenViewModel
import com.camilo.products.viewmodel.SearchScreenViewModel
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.MultiplePermissionsState
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@ExperimentalPermissionsApi
@Composable
fun ListScreen(
    navController: NavController,
    viewModel: ListScreenViewModel = hiltViewModel()
) {
    val productsList by remember { viewModel.products }
    val isLoading by remember { viewModel.isLoading }
    val error by remember { viewModel.error }

    ListScreen(navController, productsList, isLoading, error, viewModel)
}

@ExperimentalPermissionsApi
@Composable
fun ListScreen(
    navController: NavController,
    products: List<Product>,
    isLoading: Boolean,
    error: String,
    viewModel: ListScreenViewModel = hiltViewModel()
) {
    val snackBarHostState = remember { SnackbarHostState() }
    val coroutineScope = rememberCoroutineScope()

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text("Productos") },
            )
        },
        floatingActionButton = {
            Column {
                val permissionsState = rememberMultiplePermissionsState(
                    permissions = listOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                )
                FloatingActionButton(onClick = {
                    requestPermission(
                        navController,
                        coroutineScope,
                        permissionsState
                    )
                }) {
                    Icon(
                        imageVector = Icons.Filled.Add,
                        contentDescription = "",
                        tint = White
                    )
                }
                Spacer(modifier = Modifier.height(16.dp))
                FloatingActionButton(
                    onClick = {
                        navController.navigate(Destinations.SEARCH_SCREEN)
                    }) {
                    Icon(
                        imageVector = Icons.Filled.Search,
                        contentDescription = "",
                        tint = White
                    )
                }
            }
        }
    ) {
        Column {
            CircularProgressBar(isDisplayed = isLoading)
            MessageError(errorMsg = error)
            ListProduct(navController = navController, products = products)

            if (error.isNotEmpty()) {
                Box(modifier = Modifier.fillMaxSize()) {
                    coroutineScope.launch {
                        snackBarHostState.showSnackbar(message = error, actionLabel = "Ocultar")
                    }
                    SnackBarView(
                        snackHostState = snackBarHostState,
                        modifier = Modifier
                            .align(Alignment.BottomCenter)
                            .padding(20.dp), error.isNotEmpty()
                    ) {}
                    RetryView(onRetry = { viewModel.getProducts() })
                }
            }
        }
    }
}

@Composable
fun ListProduct(navController: NavController, products: List<Product>){
    LazyColumn(contentPadding = PaddingValues(4.dp)) {
        items(products) { product ->
            ProductCard(
                product = product,
                onCLick = {
                    navController.navigate("${Destinations.DETAILS_SCREEN}/${product.title}")
                }
            )
        }
    }
}

@ExperimentalPermissionsApi
private fun requestPermission(
    navController: NavController,
    coroutineScope: CoroutineScope,
    permissionsState: MultiplePermissionsState
) {

    if (permissionsState.allPermissionsGranted) {
        Toast.makeText(navController.context, "Permisos Otorgados", Toast.LENGTH_LONG).show()
    } else {
        coroutineScope.launch {
            permissionsState.launchMultiplePermissionRequest()
        }
    }
}

@ExperimentalPermissionsApi
@Preview(showBackground = true)
@Composable
fun ListPreview() {
    Product_CHTheme {
        ListScreen(
            navController = rememberNavController(),
            products = listOf(
                Product(
                    "MCO540523749", "Whisky", 12900,
                    "http://http2.mlstatic.com/D_751658-MCO46597556582_072021-I.jpg",
                    "https://articulo.mercadolibre.com.co/MCO-540523749-whisky-buchanans-deluxe-750ml-_JM"
                )
            ),
            false, ""
        )
    }
}
