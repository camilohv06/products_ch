package com.camilo.products.view.components

import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.unit.dp

@Composable
fun SnackBarView(
    snackHostState: SnackbarHostState,
    modifier: Modifier = Modifier,
    show: Boolean,
    onDismiss: () -> Unit?
) {
    if (show) {
        SnackbarHost(
            hostState = snackHostState,
            snackbar = { data ->
                Snackbar(
                    modifier = Modifier.padding(
                        bottom = 60.dp,
                        top = 2.dp,
                        end = 2.dp,
                        start = 2.dp
                    ),
                    action = {
                        data.actionLabel?.let { actionLabel ->
                            TextButton(
                                onClick = {
                                    onDismiss()
                                }
                            ) {
                                Text(
                                    text = actionLabel,
                                    style = MaterialTheme.typography.body2,
                                    color = White
                                )
                            }
                        }
                    },
                ) {
                    Text(
                        text = data.message,
                        style = MaterialTheme.typography.body2,
                        color = White
                    )
                }
            },
            modifier = modifier
        )
    }
}