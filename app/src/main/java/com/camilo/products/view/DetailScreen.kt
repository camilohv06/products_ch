package com.camilo.products.view

import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.camilo.products.R
import com.camilo.products.model.Product
import com.camilo.products.ui.theme.Product_CHTheme
import com.camilo.products.utils.Strings
import com.camilo.products.view.components.DetailProduct
import com.camilo.products.viewmodel.DetailScreenViewModel

@Composable
fun DetailsScreen(
    title: String,
    navController: NavController,
    viewModel: DetailScreenViewModel = hiltViewModel()
){
    val product by viewModel.getProductByTitle(title).observeAsState(initial = null)
    DetailsScreen(title, navController, product)
}

@Composable
fun DetailsScreen(
    title: String,
    navController: NavController,
    product: Product?
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(title, maxLines = 1) },
                navigationIcon = {
                    IconButton(onClick = { navController.popBackStack() }) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = Strings.get(R.string.lbl_back),
                        )
                    }
                }
            )
        }
    ) {
        product?.let {
            val context = LocalContext.current
            DetailProduct(product = product, onCLick = {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(product.permalink))
                context.startActivity(intent)
            })
        } ?: run {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier.fillMaxSize()
            ) {
                CircularProgressIndicator()
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DetailsPreview() {
    Product_CHTheme {
        DetailsScreen(
            title = "Product Detail",
            navController = rememberNavController(),
            product = Product("MCO540523749","Whisky",12900,
                "http://http2.mlstatic.com/D_751658-MCO46597556582_072021-I.jpg",
                "https://articulo.mercadolibre.com.co/MCO-540523749-whisky-buchanans-deluxe-750ml-_JM")
        )
    }
}