package com.camilo.products.view.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.camilo.products.model.Product
import com.camilo.products.ui.theme.Product_CHTheme
import java.text.NumberFormat

@Composable
fun DetailProduct(
    product: Product,
    onCLick: () -> Unit
) {
    Card(
        shape = RoundedCornerShape(8.dp),
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth()
            .verticalScroll(rememberScrollState()),
        elevation = 8.dp
    ) {
        Column {
            Row {
                ImageProduct(product = product)
                Column(Modifier.padding(8.dp)) {
                    Text(product.title, fontSize = 22.sp, fontWeight = FontWeight.Bold)
                    Text(
                        text = product.currency_id + " " + NumberFormat.getInstance()
                            .format(product.price),
                        fontSize = 18.sp
                    )
                    Text(
                        text = "Cantidad Disponible: " + product.available_quantity,
                        fontSize = 16.sp
                    )
                }
            }
            Box(Modifier.size(24.dp))
            Button(
                modifier = Modifier
                    .align(alignment = Alignment.CenterHorizontally)
                    .fillMaxWidth(),
                onClick = onCLick
            ) {
                Text("Ver mas")
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DetailsPreview() {
    Product_CHTheme {
        DetailProduct(
            product = Product(
                "MCO540523749", "Whisky", 12900,
                "http://http2.mlstatic.com/D_751658-MCO46597556582_072021-I.jpg",
                "https://articulo.mercadolibre.com.co/MCO-540523749-whisky-buchanans-deluxe-750ml-_JM"
            )
        ) {

        }
    }
}