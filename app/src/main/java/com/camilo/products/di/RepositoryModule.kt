package com.camilo.products.di

import com.camilo.products.provider.ProductsProvider
import com.camilo.products.repository.ProductsRepository
import com.camilo.products.repository.ProductsRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    @Singleton
    fun providerProductsProvider(productsProvider: ProductsProvider): ProductsRepository =
        ProductsRepositoryImpl(productsProvider)
}