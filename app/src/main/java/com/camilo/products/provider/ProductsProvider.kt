package com.camilo.products.provider

import com.camilo.products.model.ProductResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

private const val CATEGORY = "MCO1403"
private const val COUNTRY = "MCO"

interface ProductsProvider {
    @GET("sites/$COUNTRY/search?category=$CATEGORY")
    suspend fun getProductData(): Response<ProductResponse>

    @GET("/sites/$COUNTRY/search")
    suspend fun getProductDataByQuery(@Query("q")query: String): Response<ProductResponse>

}