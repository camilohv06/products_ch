package com.camilo.products

import android.app.Application
import android.content.Context
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ProductApplication: Application() {
    companion object {
        lateinit var instance: ProductApplication private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}