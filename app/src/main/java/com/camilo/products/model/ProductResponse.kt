package com.camilo.products.model

data class ProductResponse(
    val site_id: String,
    val results: List<Product>,
    val error: String,
    val message: String,
    val status: Int
)
