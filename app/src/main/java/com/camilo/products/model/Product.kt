package com.camilo.products.model

data class Product(
    val id: String = "",
    val title: String = "",
    val price: Int = 0,
    val thumbnail: String = "",
    val permalink: String = "",
    val currency_id: String = "COP",
    val available_quantity: Int = 0
)

