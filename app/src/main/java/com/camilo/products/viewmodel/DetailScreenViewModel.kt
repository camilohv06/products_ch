package com.camilo.products.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.camilo.products.model.Product
import com.camilo.products.repository.ProductsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailScreenViewModel @Inject constructor(
    private val repository: ProductsRepository
) : ViewModel() {

    private val _products = MutableLiveData<Product>()

    fun getProductByTitle(title:String): LiveData<Product> {
        viewModelScope.launch(Dispatchers.IO) {
            val products = repository.getProduct(title)
            _products.postValue(products)
        }
        return _products
    }
}