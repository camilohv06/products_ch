package com.camilo.products.viewmodel

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.camilo.products.model.Product
import com.camilo.products.repository.ProductsRepository
import com.camilo.products.repository.ProductsRepositoryImpl
import com.camilo.products.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchScreenViewModel @Inject constructor(
    private val repository: ProductsRepository,
) : ViewModel() {

    var productsFiltered = mutableStateOf<List<Product>>(listOf())
    var isLoading = mutableStateOf(false)
    var error = mutableStateOf("")
    var query = mutableStateOf("")

    fun getProductsByQuery(string: String) {
        if (!isValidQuery(string)) return

        isLoading.value = true
        query.value = string

        resetVariables()
        viewModelScope.launch {
            when (val result = repository.getProductsByQuery(query.value)) {
                is Resource.Success -> {
                    productsFiltered.value = result.data!!
                    delay(2000)
                    isLoading.value = false
                }
                is Resource.Error -> {
                    isLoading.value = false
                    error.value = result.message!!
                }
            }
        }
    }

    private fun resetProducts() {
        productsFiltered.value = emptyList()
    }

    private fun isValidQuery(string: String): Boolean {
        return if (string.isEmpty()) {
            resetProducts()
            false
        } else {
            string.isNotEmpty() && string != query.value
        }
    }

    private fun resetVariables() {
        isLoading.value = true
        error.value = ""
    }
}