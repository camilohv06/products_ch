package com.camilo.products.viewmodel

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.camilo.products.model.Product
import com.camilo.products.repository.ProductsRepository
import com.camilo.products.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListScreenViewModel @Inject constructor(
    private val repository: ProductsRepository,
) : ViewModel() {

    var products = mutableStateOf<List<Product>>(listOf())
    var isLoading = mutableStateOf(true)
    var error = mutableStateOf("")

    init {
        getProducts()
    }

    fun getProducts() {
        resetVariables()
        viewModelScope.launch {
            when (val result = repository.getProductsByCategory()) {
                is Resource.Success -> {
                    products.value = result.data!!
                    delay(2000)
                    isLoading.value = false
                }
                is Resource.Error -> {
                    isLoading.value = false
                    error.value = result.message!!
                }
            }
        }
    }

    private fun resetVariables(){
        isLoading.value = true
        error.value = ""
    }
}