package com.camilo.products.repository

import com.camilo.products.model.Product
import com.camilo.products.utils.Resource

interface ProductsRepository {
    suspend fun getProductsByCategory(): Resource<List<Product>>
    suspend fun getProductsByQuery(query: String): Resource<List<Product>>
    fun getProduct(title: String): Product
}