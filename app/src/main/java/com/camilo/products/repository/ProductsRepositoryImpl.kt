package com.camilo.products.repository

import com.camilo.products.R
import com.camilo.products.model.Product
import com.camilo.products.model.ProductResponse
import com.camilo.products.provider.ProductsProvider
import com.camilo.products.utils.Resource
import com.camilo.products.utils.Strings
import kotlinx.coroutines.flow.merge
import retrofit2.Response
import java.net.UnknownHostException
import javax.inject.Inject

class ProductsRepositoryImpl @Inject constructor(
    private val api: ProductsProvider
) : ProductsRepository {

    private var products: List<Product> = emptyList()
    private var productsFiltered: List<Product> = emptyList()

    override suspend fun getProductsByCategory(): Resource<List<Product>> {
        val response: Response<ProductResponse>
        try {
            response = api.getProductData()

            when (response.raw().code) {
                200 -> {
                    if (response.body()!!.results.isEmpty()) {
                        return Resource.Error(Strings.get(R.string.error_no_products))
                    }
                }
                else -> return Resource.Error(Strings.get(R.string.error_system))
            }

        } catch (e: UnknownHostException) {
            return Resource.Error(Strings.get(R.string.error_internet))
        } catch (e: Exception) {
            return Resource.Error(Strings.get(R.string.error, e.localizedMessage))
        }

        products = response.body()!!.results
        return Resource.Success(products)
    }

    override suspend fun getProductsByQuery(query : String): Resource<List<Product>> {
        val response: Response<ProductResponse>
        try {
            response = api.getProductDataByQuery(query)
            processResponse(response)
        } catch (e: UnknownHostException) {
            return Resource.Error(Strings.get(R.string.error_internet))
        } catch (e: Exception) {
            return Resource.Error(Strings.get(R.string.error, e.localizedMessage))
        }

        productsFiltered = response.body()!!.results
        return Resource.Success(productsFiltered)
    }

    private fun processResponse(response: Response<ProductResponse>): Resource<List<Product>> {
        when (response.raw().code) {
            200 -> {
                if (response.body()!!.results.isEmpty()) {
                    return Resource.Error(Strings.get(R.string.error_no_products))
                }
            }
            else -> return Resource.Error(Strings.get(R.string.error_system))
        }
        return Resource.Error(Strings.get(R.string.error_system))
    }


    override fun getProduct(title: String): Product {
        products.forEach {
            if (it.title == title){
                return it
            }
        }
         productsFiltered.forEach {
            if (it.title == title){
                return it
            }
        }
        return Product();
    }
}